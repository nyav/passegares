import requests
from pyproj import Transformer

class Lambert93Convert:
    def convert(self, x, y):
        transformer = Transformer.from_crs("epsg:2154", "epsg:4326") # Lambert 93 => WGS84
        latitude, longitude = transformer.transform(x, y)

        return float(latitude), float(longitude)
