import regionManager


class RegionManagerFactory:
    def __init__(self, config, metadonnees):
        self.config = config
        self.metadonnees = metadonnees
        self.regionManager = {}

    def getRegionManager(self, dossierId, regionName):
        if not dossierId in self.regionManager.keys():
            self.regionManager[dossierId] = regionManager.RegionManager(
                self.config, dossierId, regionName, self.metadonnees)

        return self.regionManager[dossierId]
